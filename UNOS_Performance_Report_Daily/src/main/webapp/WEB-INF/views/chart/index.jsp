<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.common.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.moonlight.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.moonlight.mobile.min.css" />
<script src="${ctxPath }/js/kendo.all.min.js"></script>


<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	
	
	function setToday(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		
		$(".date").val(year + "-" + month + "-" + day);
		$(".time").val(hour + ":" + minute);
	};

	
	function upDateS(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		//changeDateVal();
	};
	
	function downDateS(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		//changeDateVal();
	};
	
	function upDateE(){
		var $date = $("#eDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#eDate").val(today);
		//changeDateVal();
	};
	
	function downDateE(){
		var $date = $("#eDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#eDate").val(today);
		//changeDateVal();
	};
	
	
	$(function(){
		$("#upS").click(upDateS);
		$("#downS").click(downDateS);
		$("#upE").click(upDateE);
		$("#downE").click(downDateE);
		
		if(sDate==""){
			sDate = window.localStorage.getItem("jig_sDate");
			eDate = window.localStorage.getItem("jig_eDate");
		};
		
		createNav("analysis_nav", 1);
		
		setEl();
		setDate();	
		time();
		
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		getDvcList();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").not(".dateController").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").not(".dateController").css({
			"padding" : getElSize(15),
		})
		
		
		$(".dateController").css({
			"width" : getElSize(70) *  1.2,
			"height" : getElSize(70),
		});
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#arrow_left").css({
			"width" : getElSize(60),
			"margin-right" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#arrow_right").css({
			"width" : getElSize(60),
			"margin-left" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("img").css({
			"display" : "inline"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		if(getParameterByName('lang')=='ko'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getDvcList(){
		var url = "${ctxPath}/getJigList4Report.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(json){
				var json = json.dvcList;
				
				var tr = "";
				var options = "";
				$(json).each(function(idx, data){
					tr += "<tr class='dvcTr'>" + 
								"<td >" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</td>" + 
						"</tr>"
						
					options += "<option value='" + decodeURIComponent(data.name).replace(/\+/gi,' ') + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";  
				});
				
				//$("#dvcList").html(tr);
				$("#dvcList").html(options);
				
			
				if(!first_load){
					$("#dvcList").val(dvc);
				};

				first_load = false;	
				$("#dvcList").val(dvc);
				
				
				if(dvc=="") {
					$("#dvcList").val($("#dvcList option:nth(0)").html());
					dvc = $("#dvcList option:nth(0)").html();
				}
				
				showWcDatabyDvc();
				/* $("#dvcList").change(function(){
					showWcDatabyDvc();
				}); */
				
			}
		});
	};

	var color;
	var first_load = true;
	function showWcDatabyDvc(dvc, sDate, eDate, ty){
		var dvc = $("#dvcList").val();
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		
		var url = "${ctxPath}/getWcDataByDvc.do";
		var param = "name=" + dvc + 
					"&sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&shopId=" + shopId;
		
		console.log(url + "?" + param);

		
		if(ty=="jig"){
			$("#wc_sdate").val($("#jig_sdate").val());
			$("#wc_edate").val($("#jig_edate").val());		
		};
		
		if($("#sDate").val() > $("#eDate").val()){
			alert("시작 날짜가 더 최근일 수 없습니다.");
			return;
		}
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.wcList;
				
				
				console.log(json)
				
				var start = new Date(sDate);
				var end = new Date(eDate);
				var n = (end - start)/(24 * 3600 * 1000)+1;

				var tr = "";

				wcList = new Array();
				var wc = new Array();
				
				wc.push("${division}");
				wc.push("${ophour}");
				wc.push("${wait}");
				wc.push("${stop}");
				wc.push("${noconnection}");
				wc.push("${production}");
				
				wcList.push(wc);
				
				dateList = new Array();
				inCycleBar = new Array();
				waitBar = new Array();
				alarmBar = new Array();
				noConnBar = new Array();
				cBarPoint = 0;
				cPage = 1;
				
				//console.log(json)
				
				$(json).each(function(idx, data){
					dateList.push(data.workDate.substr(5).replace("-","/"));
					tmpArray = dateList;
					
					var wc = new Array();
					wc.push(data.workDate.substr(5).replace("-","/"));
					
					
					var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
					var wait = Number(Number(data.wait_time/60/60).toFixed(1));
					var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
					var noconn = Number(Number(data.noConnTime/60/60)).toFixed(1);
					
					
					wc.push(incycle);
					wc.push(wait);
					wc.push(alarm);
					wc.push(noconn);
					wc.push(data.production);
					
					wcList.push(wc);
					tmpWcList = wcList;
					
					
					inCycleBar.push(incycle);
					waitBar.push(wait);
					alarmBar.push(alarm);
					noConnBar.push(Number(noconn));
					
					tmpInCycleBar = inCycleBar;
					tmpWaitBar = waitBar;
					tmpAlarmBar = alarmBar;
					tmpNoConnBar = noConnBar;
				});
			
				//console.log("wcList`s length : " + wcList.length)
				
				
				var blank = maxBar - json.length
				maxPage = json.length - maxBar; 
				
				var size = wcList.length
				 
				check22=true
				
				var multiple = 1
				var result
				while(check22){
					
					if((multiple*11)>size){
						
						result=(multiple*11)-size
						
						/* console.log(multiple*11)
						console.log(size) */
						
						check22=false
					}else{
						multiple++;
					}
					
				}
				
				//console.log(result)
					
				/* for(var i = 0; i < result; i++){
					dateList.push("");
					var wc = new Array();
					wc.push("____");
					wc.push("");
					wc.push("");
					wc.push("");
					wc.push("");
					
					wcList.push(wc);
					
					inCycleBar.push(0);
					waitBar.push(0);
					alarmBar.push(0);
					noConnBar.push(0);
				}; */
				
				/* for(var i = 0; i < maxBar - json.length % 10; i++){
					dateList.push("");
					var wc = new Array();
					wc.push("____");
					wc.push("");
					wc.push("");
					wc.push("");
					wc.push("");
					
					wcList.push(wc);
					
					inCycleBar.push(0);
					waitBar.push(0);
					alarmBar.push(0);
					noConnBar.push(0);
				}; */
				
				if(json.length > maxBar){
					overBar = true;
					
				};
				
				//reArrangeArray();
				
				//console.log(wcList)
				$(".chartTable").remove();
				
				
				startI=0;
				startJ=0;
				
				if(wcList.length>11){
					jMaxLength=11	
				}else{
					jMaxLength=wcList.length
				}
					
				
				
				var data = [];
				var y=0;
				var check=false;
				var check1=false;
				var lastNum=0;
				
				console.log(wcList)
				
				
				//보여줄 페이지로 나눔
				var size2=0
				if( ((wcList.length/11) - (Math.floor(wcList.length/11))) >0 ){
					size2 = (Math.floor(wcList.length/11))+2
				}
				
				//"no1"필드가 제목 구분 줄 이므로 j-a+1 로 다시 계산하게 되는데 j(행의 번호), a(페이지)
				var size3 =0;
				for(var a=0;a<size2;a++){
					for(var i=0;i<wcList[0].length-1;i++){
						data[y++]={}
						for(var j=startJ;j<jMaxLength;j++){
							if(check){
								data[y-1]["no1"]=wcList[0][i+1]
								check1=true
							}
							if(check1){
								/* console.log(startJ)
								console.log("j : " + j + ", a : " + a + ", j-a+1 : " + (j-a+1))
								console.log("[1] : " + (y-1) + ", no" + (j%11+2) + " : " + wcList[j-a+1][i+1]) */
								data[y-1]["no"+(j%11+2)]=wcList[j-a+1][i+1]
								size3=j-a+1
							}else{
								data[i]["no"+(j+1)]=wcList[j][i+1]
							}
						}					
					}
					
					if(size3+11>=wcList.length){
						
						jMaxLength =  jMaxLength + (wcList.length - size3)
						
					}else{
						jMaxLength+=11
					}
					
					startJ+=11
					check=true
				}
				
				for(var i=data.length-1;i>=0;i--){
					if(data[i].no1){
					}else{
						data.splice(i)
					}				
				}
				
				$("#tableContainer").kendoGrid({
		            dataSource: {
		                data: data
		            },
		            pageable: {
		            	pageSize: 5,
		            	buttonCount:5,
		            	info: false,
		            	alwaysVisible: false,
	            	    change: function(e){
				            chart("chart", e.index);
			            },
		            },
		            height: getElSize(525),
		            scrollable: false,
		            columns:[{
		            		field:"no1",title:"  ",width:getElSize(180),attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30) + "; font-weight : bolder; color : white;"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no2",title:" ",width:getElSize(180),attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30) + "; font-weight : bolder; color : white;"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no3",title:" ", width:getElSize(180), attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30) + "; font-weight : bolder; color : white;"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no4",title:" ",width:getElSize(180) , attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30) + "; font-weight : bolder; color : white;"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no5",title:" ", width:getElSize(180) ,attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30) + "; font-weight : bolder; color : white;"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no6",title:" ", width:getElSize(180), attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30) + "; font-weight : bolder; color : white;"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no7",title:" ", width:getElSize(180), attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30) + "; font-weight : bolder; color : white;"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no8",title:" ", width:getElSize(180), attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30) + "; font-weight : bolder; color : white;"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no9",title:" ", width:getElSize(180), attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30) + "; font-weight : bolder; color : white;"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no10",title:" ", width:getElSize(180), attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30) + "; font-weight : bolder; color : white;"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    },{
		       		    	field:"no11",title:" ", width:getElSize(180), attributes: {
		            			style: "text-align: center; font-size:" + getElSize(30) + "; font-weight : bolder; color : white;"
		            			},headerAttributes: {
		            				style: "text-align: left; font-size:" + getElSize(45)
		            			}
		       		    }]
		            });
					
				
				$("td:contains('${production}'):last").css({
					"background-color" :  "#3496F6",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});	
				$("td:contains('${ophour}'):last").css({
					//"background-color" :  "#A3D800",
					"background-color" :  "#50ba29",
					
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});	
				
				$("td:contains('${wait}'):last").css({
					//"background-color" :  "#FF9100",
					"background-color" :  "#ffff00",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});	
				
				$("td:contains('${stop}'):last").css({
					"background-color" :  "#C41C00",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});	
				
				$("td:contains('${noconnection}'):last").css({
					"background-color" :  "#6B6C7C",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
				});	
				
				/* var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
				
				for(var i = 0; i < wcList[0].length; i++){
					table += "<tr class='contentTr'>";
					
					var bgColor, color;
					if(i==1){
						bgColor = incycleColor;
						color = "black";
					}else if(i==2){
						bgColor = waitColor;
						color = "black";
					}else if(i==3){
						bgColor = alarmColor;
						color = "black";
					}else if(i==4){
						bgColor = noconnColor;
						color = "black";
					}else{
						bgColor = "#323232";
					}
					
					for(var j = 0; j < wcList.length; j++){
						if(j==0){
							table += "<td class='title_' style='font-weight: bolder;  background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
						}else if(j==0 || i==0){
							table += "<td class='title_' style='font-weight: bolder;  background-color: " + bgColor + "; color:" + color + "; width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
						}else {
							var n;
							if(typeof(wcList[j][i])=="number"){
								n = Number(wcList[j][i]/60/60).toFixed(1)
							}else{
								n = "";
							};
							table += "<td class='content'>" + n + "</td>";
						};
					};
					table += "</tr>";
				};
				
				table += "</table>";
				$("#tableContainer").append(table) */
				
				$(".title_").css({
					"width" : getElSize(300),
					"padding" : getElSize(5),
					"font-size" : getElSize(40)
				});
				
				$(".content").css({
					"font-size" : getElSize(40),
					"width" : ($(".contentTr").width() - getElSize(300))/10
				})
				
				//setEl();
				$('.k-pager-nav').hide();
				chart("chart", 0);
				//addSeries();
				
				//$("#tableContainer").css("margin-top",$("#chart").offset().top + $("#chart").height() - $(".mainTable").height() + getElSize(50))
			}
		});
	};
	
	var xAxis = new Array();
	var barChart;
	
	
	function chart(id, index){
		
		if(index>0){
			index-=1
		}
		
		var actualIncycleBar = new Array();
		var actualWaitBar = new Array();
		var actualAlarmBar = new Array();
		var actualDateList = new Array();
		
		
		var showInCycleBar = []
		var showWaitBar = []
		var showAlarmBar = []
		var showDateList = []
		
		for(var i=0;i<inCycleBar.length;i++)
		actualIncycleBar.push(inCycleBar[i])
		
		for(var i=0;i<waitBar.length;i++)
		actualWaitBar.push(waitBar[i])
		
		for(var i=0;i<alarmBar.length;i++)
		actualAlarmBar.push(alarmBar[i])
		
		/* for(var i=0;i<dateList.length;i++)
		actualDateList.push(dateList[i]) */
		
		showInCycleBar = actualIncycleBar.splice(index*10, 10)
		showWaitBar = actualWaitBar.splice(index*10, 10)
		showAlarmBar = actualAlarmBar.splice(index*10, 10)
		//showDateList = actualDateList.splice(index*10,10)
		
		var size = 0;
		if(index*10>dateList.length){
			size = dateList.length
		}else{
			size = index*10+10
		}
		
		
		
		/* console.log("index : " + index)
		console.log("size : " + size) */
		
		for(var i=index*10;i<size;i++){
			/* console.log(i) */
			actualDateList.push(dateList[i])
		}
		
		
		/* console.log("actualDateList")
		console.log(actualDateList)
		console.log("---------------------------") */
		
		console.log(showInCycleBar)
		console.log(showWaitBar)
		console.log(showAlarmBar)
		
		var series = [{
	            data: showInCycleBar,
	            //color: "#A3D800",
	            color: "#50ba29",
	            
	            // Line chart marker type
	            markers: { type: "square" },
        	},{
				data: showWaitBar,
				//color: "#FF9100"
				color: "#ffff00"
       		},{
	            data: showAlarmBar,
	            color: "#C41C00"
          	}];
		
		//console.log(series)
		$("#" + id).kendoChart({
		    theme: "materialBlack",
		    seriesDefaults: {
                type: "column",
                stack: true, 
                labels: {
                    visible: false,
                    position: "bottom",
                    background: "transparent"
                }
            },
		    series : series,
		    axisDefaults:{
		    	labels: {
	    			format: "{0}",
	                step: 1
	              },
		    },
		    valueAxis: {
	              min:0,
	              max:24,
	              majorUnit: 4
	            },
		    chartArea: {
                width: getElSize(3320),
            	height: getElSize(1100),
            	margin: {
        	      left: getElSize(250)
        	    }
            },
		    categoryAxis:{
		        categories: actualDateList
		    }
		});
		
		
		
		
		/* var sDate = window.localStorage.getItem("sDate");
		var eDate = window.localStorage.getItem("eDate");
		
		var date = new Date(sDate);
		
		var margin = (originWidth*0.05)/2;
		$("#" + id).css({
			"margin-left" : $("#tableContainer table td:nth(1)").offset().left - margin- getElSize(150) - $(".menu_left").width(),
			//"margin-left" : $("#tableContainer table td:nth(1)").offset().left - margin - getElSize(80),
			"width" : $("#tableContainer table").width() - $("#tableContainer table td:nth(0)").width()-getElSize(20) + getElSize(80),
			"top" : $("#content_table").offset().top + $("#content_table").height() + getElSize(50)
		})
		
		
		$('#' + id).highcharts({
	        chart: {
	            type: 'column',
	            backgroundColor : 'rgb(50, 50, 50)',
	            height :getElSize(1150),
	            width : getElSize(3180),
	            marginLeft:getElSize(150),
	            marginRight:0,
	            marginBottom : getElSize(250)
	        },
	        title: {
	            text:false
	        },
	        xAxis: {
	            categories: dateList,
	            labels : {
	            	style : {
	            		"color" : "white",
	            		"font-size" : getElSize(30)
	            	}
	            }
	        },
	        yAxis: {
	            min: 0,
	            max : 24,
	            title: {
	                text: "Hour (h)",
	                style:{
	                	color : "white"
	                }
	            },
	            labels : {
	            	style : {
	            		"color" : "white",
	            		"font-size" : getElSize(30)
	            	},
	            	enabled:true
	            },
	            //reversed:true 
	        },
	        tooltip: {
	            enabled : false
	        },
	        plotOptions: {
	            column: {
	                stacking: 'normal',
	                dataLabels: {
	                    enabled: true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
	                    style: {
	                        //textShadow: '0 0 3px black',
	                    }
	                }
	            },
	            series : {
	            	pointWidth:getElSize(100)
	            }
	        },
	        credits : false,
	        exporting : false,
	        legend:{
	        	enabled : false
	        },
	        series: []
	    });
		
		barChart = $("#" + id).highcharts(); */
	}
	
	
	var incycleColor = "#50BA29";
	var cuttingColor = "#175501";
	var waitColor = "yellow";
	var alarmColor = "red";
	var noconnColor= "#A0A0A0";
	
	function addSeries(){
		barChart.addSeries({
			color : "gray",
			data : noConnBar
		}, true);

		barChart.addSeries({
			color :alarmColor,
			data : alarmBar
		}, true);
		
		barChart.addSeries({
			color : waitColor,
			data : waitBar
		}, true);
		
		barChart.addSeries({
			color : incycleColor,
			data : inCycleBar
		}, true);
	};
	
	var jig = window.localStorage.getItem("jig");
	var dvc = replaceHyphen("${dvcName}");

	var chartWidth;
	var maxBar = 10;
	var tmpArray = new Array();
	var tmpInCycleBar = new Array();
	var tmpWaitBar = new Array();
	var tmpAlarmBar = new Array();
	var tmpNoConnBar = new Array();
	var tmpWcName = new Array();
	var tmpWcList = new Array();

	function reArrangeArray(){
		dateList = new Array();
		inCycleBar = new Array();
		waitBar = new Array();
		alarmBar = new Array();
		noConnBar = new Array();
		wcName = new Array();
		wcList = new Array();
		
		for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
			dateList[i-cBarPoint] = tmpArray[i];
			inCycleBar[i-cBarPoint] = tmpInCycleBar[i];
			waitBar[i-cBarPoint] = tmpWaitBar[i];
			alarmBar[i-cBarPoint] = tmpAlarmBar[i];
			noConnBar[i-cBarPoint] = tmpNoConnBar[i];
			wcName[i-cBarPoint] = tmpWcName[i];
		};
		
		for(var i = cBarPoint; i < (maxBar+cBarPoint+1); i++){
			wcList[i-cBarPoint] = tmpWcList[i];	
		};
		var wc = new Array();
		wc.push("${division}");
		wc.push("${ophour}");
		wc.push("${wait}");
		wc.push("${stop}");
		wc.push("${noconnection}");
		
		//wcList.push(wc);
		wcList[0] = wc;
		
	};

	var cPage = 1;
	
	function nextBarArray(){
		if(cBarPoint>=maxPage || maxPage<=0){
			alert("${end_of_chart}");
			return;
		};
		cBarPoint += maxBar;
		cPage++;
		reArrangeArray();
		
		$(".chartTable").remove();
		var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		
		for(var i = 0; i < wcList[0].length; i++){
			table += "<tr class='contentTr'>";
			
			var bgColor,color;
			if(i==1){
				bgColor = incycleColor;
				color = "black";
			}else if(i==2){
				bgColor = waitColor;
				color = "black";
			}else if(i==3){
				bgColor = alarmColor;
				color = "black";
			}else if(i==4){
				bgColor = noconnColor;
				color = "black";
			}else{
				bgColor = "#323232";
			}
			
			
			for(var j = 0; j < wcList.length; j++){
				if(j==0){
					table += "<td class='title_' style='font-weight: bolder;   background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
				}else if(j==0 || i==0){
					table += "<td class='title_' style='font-weight: bolder;  background-color: " + bgColor + "; color:" + color + "; width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
				}else {
					var n;
					if(typeof(wcList[j][i])=="number"){
						n = Number(wcList[j][i]/60/60).toFixed(1)
					}else{
						n = "";
					};
					table += "<td class='content'>" + n + "</td>";
				};
			};
			table += "</tr>";
		};
		
		table += "</table>";
		$("#tableContainer").append(table)
		
		$(".title_").css({
					"width" : getElSize(300),
					"padding" : getElSize(5),
					"font-size" : getElSize(40)
				});
				
		$(".content").css({
			"font-size" : getElSize(40),
			"width" : ($(".contentTr").width() - getElSize(300))/10
		})
				
		//setEl();
		chart("chart");
		addSeries();
	};

	function prevBarArray(){
		if(cBarPoint<=0){
			alert("${first_of_chart}");
			return;
		};
		cBarPoint -= maxBar;
		cPage--;
		reArrangeArray();
		
		$(".chartTable").remove();
		var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		
		for(var i = 0; i < wcList[0].length; i++){
			table += "<tr class='contentTr'>";
			
			var bgColor;
			if(i==1){
				bgColor = incycleColor;
				color = "black";
			}else if(i==2){
				bgColor = waitColor;
				color = "black";
			}else if(i==3){
				bgColor = alarmColor;
				color = "black";
			}else if(i==4){
				bgColor = noconnColor;
				color = "black";
			}else{
				bgColor = "#323232";
			}
			
			
			for(var j = 0; j < wcList.length; j++){
				if(j==0){
					table += "<td class='title_' style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
				}else if(j==0 || i==0){
					table += "<td class='title_' style='font-weight: bolder;background-color: " + bgColor + "; color:" + color + "; width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
				}else {
					var n;
					if(typeof(wcList[j][i])=="number"){
						n = Number(wcList[j][i]/60/60).toFixed(1)
					}else{
						n = "";
					};
					table += "<td class='content'>" + n + "</td>";
				};
			};
			table += "</tr>";
		};
		
		table += "</table>";
		$("#tableContainer").append(table)
		
		$(".title_").css({
					"width" : getElSize(300),
					"padding" : getElSize(5),
					"font-size" : getElSize(40)
				});
				
		$(".content").css({
			"font-size" : getElSize(40),
			"width" : ($(".contentTr").width() - getElSize(300))/10
		})
		
		//setEl();
		chart("chart");
		addSeries();
	};
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	var maxPage;
	var cBarPoint = 0;
	var overBar = false;
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	
	<div id="time"></div>
	<div id="title_right"></div>
	
	<div id="dashBoard_title">${operation_graph_daily_title}</div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/analysis_left.png" class='menu_left'  style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/green_right.png" class='menu_right' style="display: none">
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left' style="display: none">
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td> 
								<spring:message code="device"></spring:message>
								<select id="dvcList"></select>
								<spring:message code="op_period"  ></spring:message>
								<button id="upS" class='dateController'>▲︎</button><button id="downS" class='dateController'>▼</button>
								<input type="date" class="date" id="sDate"> 
								~
								<button id="upE" class='dateController'>▲︎</button><button id="downE" class='dateController'>▼</button>
								<input type="date" class="date" id="eDate">
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="showWcDatabyDvc()">
							</td>
						</Tr>		
						<tr>
							<td>
								<div >
									<div id="chart" style="width: 100%">
										
									</div>
									<div id="tableContainer" style="width: 100%"></div>
								</div>									
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	